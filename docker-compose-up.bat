@echo off
FOR /f "tokens=*" %%i IN ('docker-machine env') DO %%i
@echo on
docker-compose up -d
docker-compose logs -f
