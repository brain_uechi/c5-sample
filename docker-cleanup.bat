@echo off
FOR /f "tokens=*" %%i IN ('docker-machine env') DO %%i
@echo on
docker image prune
cmd /k docker volume prune

