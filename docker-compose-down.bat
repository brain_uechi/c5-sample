@echo off
FOR /f "tokens=*" %%i IN ('docker-machine env') DO %%i
@echo on
docker-compose down
cmd /k docker volume prune
